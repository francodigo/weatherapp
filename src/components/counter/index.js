import {h} from 'preact';

import { useState } from 'preact/hooks';
import style from './style.css';

const Counter = () => {

    const [count, setCount] = useState(0);

    const increment = () => setCount(count + 1);
    const decrement = () => {
        if (count > 0){
            setCount((currentCount) => currentCount - 1);
        }
    }

    return (
        <div class={style.counter}>
            <h1>This is a Counter</h1>
            <p>Welcom to the Counter</p>
            <p>{count}</p>
            <button class={style.counter__btnAdd}onClick={increment}>+</button>
            <button class={style.counter__btnDelete}onClick={decrement}>-</button>

        </div>
    )
}

export default Counter;