import { h } from 'preact';
import style from './style.css';

import Counter from '../../components/counter'

const Home = () => (
	<div class={style.home}>
		<h1>Home</h1>
		<p>This is the Home component.</p>
		<Counter></Counter>
	</div>
);

export default Home;
